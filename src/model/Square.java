package model;

import javax.swing.SwingUtilities;

import model.entity.Entity;
import model.entity.Vehicle;

public class Square implements Comparable<Square> {

    private int xCo, yCo;
    private SquareType type;
    private Field field;
    
    private Entity entity;
    
    // needed for A* pathfinding
    private Square parent;
    private int movementCost = -1;
    private int heuristic = -1;
    
    public Square() {
        this(SquareType.NONE);
    }
    
    public Square(SquareType type) {
        this.type = type;
    }  
    
    public int getXCo() { return this.xCo; }
    public void setXCo(int xCo) { this.xCo = xCo; }
    
    public int getYCo() { return this.yCo; }
    public void setYCo(int yCo) { this.yCo = yCo; }
    
    public void setField(Field field) { this.field = field; }
    public Field getField() { return this.field; }
       
    public int getValue() { return type.getValue(); }
    
    public SquareType getType() { return this.type; }
    public void setType(SquareType type) { this.type = type; }
    
    public Square getParent() { return this.parent; }
    public void setParent(Square parent) { this.parent = parent; }
    
    public Entity getEntity() { return this.entity; }
    public synchronized void setEntity(Entity e) {
        this.entity = e;
        if (e != null && e.getPosition() != this) {
            e.setPosition(this);
        }
    }
    
    
    public boolean isWalkable() { return type.isWalkable(); }
    
    public synchronized boolean isOccupied() { return (getEntity() != null); }
    
    public int getMovementCost() { return movementCost; }
    public void setMovementCost(int g) { this.movementCost = g; }
    
    public int getHeuristic() { return this.heuristic; }
    public void setHeuristic(int h) { this.heuristic = h; }
     
    public void reset() {
        this.parent = null;
        this.movementCost = -1;
        this.heuristic = -1;
    }
    
    
    /**
     * OBSERVER pattern - extension
     * Notification is delegated to field, which notifies its observers. 
     * This 'square' object is given as value to be repainted
     */
    public void notifyField() {
        field.setChanged();
        field.notifyObservers(this); 
      
    }
        
    public boolean equals(Square s) {
        return (s.getXCo() == this.getXCo()) && (s.getYCo() == this.getYCo());
    }
    
    @Override
    public int hashCode() {
        return getXCo() + getYCo();
    }
         
    @Override
    public String toString() {
        return "[(" + getXCo() + ":" + getYCo() + ") " + getValue() + "]";
    }
          
    @Override
    public int compareTo(Square s) {
        return (getMovementCost() + getHeuristic()) - (s.getMovementCost() + s.getHeuristic()); 
    }
    
}
