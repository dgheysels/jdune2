package model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Responsible for calculating a path from a start-square 
 * to a destination-square
 * 
 */
public class GPS {

    private Field field;
    private Square start;
    private Square destination;
    
    public GPS(/*Field field*/) {
        setField(FieldGenerator.getGeneratedField());      
    }
    
    public Square getStart() { return this.start; }    
    public void setStart(Square start) { this.start = start; }
    
    public Square getDestination() { return this.destination; }
    public void setDestination(Square destination) { this.destination = destination; }
    
    public Field getField() { return this.field; }
    public void setField(Field field) { this.field = field; }
    
    public void reset() {
    	this.start = null;
    	this.destination = null;
    }
    
    public void printpath(List<Square> path) {
        Iterator<Square> it = path.iterator();
        
        while (it.hasNext()) {
            System.out.print(" -> " + it.next());
        }
    }
        
    /*
     * A* pathfinding algorithm
     * http://www.policyalmanac.org/games/aStarTutorial.htm
     */   
    public List<Square> findpath() {   
        synchronized(field) {
            field.reset();
            
            if (getStart() != null && getDestination() != null && 
                    getStart().isWalkable() && getDestination().isWalkable()) {
                PriorityQueue<Square> openlist = new PriorityQueue<Square>();                
                HashSet<Square> closedlist = new HashSet<Square>(field.getWidth() * field.getHeight());
                
                openlist.offer(getStart());

                // find optimal path
                while (!(openlist.isEmpty() || closedlist.contains(getDestination()))) {
                    Square best = openlist.poll();
                    closedlist.add(best);

                    HashSet<Square> neighbours = field.getNeigbours(best); 
                    for (Square n : neighbours) {                      
                        if (!closedlist.contains(n)) {
                            if (openlist.contains(n)) {
                                // determine if there is no better path
                                // via square 'n'
                                int g = n.getValue() + getMovementCost(best);
                                if (g < getMovementCost(n)) {
                                    n.setParent(best);      
                                }
                            }
                            else {
                                n.setParent(best);   
                                n.setMovementCost(getMovementCost(n));
                                n.setHeuristic(getHeuristic(n));

                                openlist.offer(n);
                            }
                        }              
                    }         
                } 

                // construct path
                LinkedList<Square> path = new LinkedList<Square>();
                Square current = getDestination();
                path.addFirst(current);

                while (current.getParent() != null) {
                    current = current.getParent();
                    path.addFirst(current);
                }

                // check if it is a proper path.  
                // Ie. no walls between start and destination
                if (path.peekFirst().equals(getStart())) {
                    // delete start-square
                    path.removeFirst();
                    
                    return path;
                }
                else {
                    return Collections.emptyList();
                }  
            }
            else {
                return Collections.emptyList();
            }
        }
    }
    
    private int getMovementCost(Square s) {
        if (s.getParent() != null) {
            return s.getValue() + getMovementCost(s.getParent());
        }
        else { // movement cost of start-square is irrelevant
            return 0;
        }
    }
 
    /*
     * http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#S1
     * 
     * lower value = slower pathfinding, but optimal
     * higher value = faster pathfinding, but not guaranteed to be optimal
     * 
     * heuristic should always be lower than (or equal) to the real cost of going to the destination square
     *  == admissable heuristic
     */
    private int getHeuristic(Square s) {
       return 4 * 
               (Math.abs(getDestination().getXCo() - s.getXCo()) + 
                Math.abs(getDestination().getYCo() - s.getYCo()));
    }
    
   private double f(Square s) {
       return getMovementCost(s) + getHeuristic(s);
   }
}
