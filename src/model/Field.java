package model;

import gui.component.terrain.View;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;

import model.entity.Building;
import model.entity.Entity;
import model.entity.Vehicle;
import model.entity.command.Command;

public class Field extends Observable {

    private Square[][] squares;
    private View view;
    
    
    private Entity selectedEntity;
        
    public Field(int width, int height) {
        squares = new Square[width][height];
    }
       
    public void reset() {
        for (int y=0; y<getHeight(); y++) {
            for (int x=0; x<getWidth(); x++) {
                squares[x][y].reset();
            }
        }
    }
    
    public int getHeight() { return squares[0].length; }
    public int getWidth() { return squares.length; }
    
    public Square getSquare(int x, int y) {
        return squares[x][y];
    }

    public void setSquare(int x, int y, Square s) {
        s.setXCo(x);
        s.setYCo(y);
        s.setField(this);
        squares[x][y] = s;
    }   
    
    public Entity getSelectedEntity() {
        return selectedEntity;
    }
    
    public void setSelectedEntity(Entity entity) {
        this.selectedEntity = entity;
        setChanged();
        notifyObservers();
    }
    
    public void addEntity(Entity entity, int xCo, int yCo) {
        getSquare(xCo, yCo).setEntity(entity);
    }
    
    public void removeEntity(Entity entity) {
    	entity.getPosition().setEntity(null);       
    	entity.setPosition(null);
    }
    
       
     /**
     *  get absolute square coordinates 
     *  coordinate = position of viewport + coordinate of the viewable square in window
     */
    public int getAbsoluteXCoordinate(int screenXCo) {
        return view.getXPosition() + view.calculateXCoordinate(screenXCo);
    }

    public int getAbsoluteYCoordinate(int screenYCo) {
        return view.getYPosition() + view.calculateYCoordinate(screenYCo);
    }
    
    public View getView() { return this.view; }
    public void setView(View view) { this.view = view; }
   
    @Override
    public void setChanged() {
        super.setChanged();
    }
    
    /**
     * retrieve neighbours of square
     * only left/right/top/bottom squares are neighbours
     */
    public HashSet<Square> getNeigbours(Square s) {
        HashSet<Square> result = new HashSet<Square>();
        int x = s.getXCo();
        int y = s.getYCo();

        if (x == 0) {
            if (y == 0) {
                result.add(squares[x+1][y]);
                result.add(squares[x][y+1]);

            }
            else if (y == squares[0].length-1) {
                result.add(squares[x+1][y]);
                result.add(squares[x][y-1]);
            }
            else {
                result.add(squares[x][y-1]);
                result.add(squares[x+1][y]);
                result.add(squares[x][y+1]);
            }
        }
        else if (x == squares.length-1) {
            if (y == 0) {
                result.add(squares[x-1][y]);
                result.add(squares[x][y+1]);     
            }
            else if (y == squares[0].length-1) {
                result.add(squares[x-1][y]);
                result.add(squares[x][y-1]);
            }
            else {
                result.add(squares[x][y-1]);
                result.add(squares[x-1][y]);
                result.add(squares[x][y+1]);
            } 
        }
        else {
            if (y == 0) {
                result.add(squares[x-1][y]);
                result.add(squares[x][y+1]); 
                result.add(squares[x+1][y]); 
            }
            else if (y == squares[0].length-1) {
                result.add(squares[x-1][y]);
                result.add(squares[x][y-1]);
                result.add(squares[x+1][y]); 
            }
            else {
                result.add(squares[x-1][y]);
                result.add(squares[x+1][y]);
                result.add(squares[x][y+1]);
                result.add(squares[x][y-1]);     
            }  
        }
        
        Iterator<Square> i = result.iterator();
        
        // remove all neighbours which are invalid
        // invalid = square is not walkable or square contains a building
        while (i.hasNext()) {
        	Square squareToInvestigate = i.next();
            if (!squareToInvestigate.getType().isWalkable() || 
            	(squareToInvestigate.isOccupied() && (squareToInvestigate.getEntity() instanceof Building))) {
                i.remove();
            }
        }
        
        return result;
    }
       
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        for (int y=0; y<getHeight(); y++) {
            for (int x=0; x<getWidth(); x++) {
                sb.append(getSquare(x, y));
                sb.append("\t \t \t");
            }
            sb.append("\n");
        }
        
        return sb.toString();
    }
    
    public void paint(Graphics2D g) {
    	DisplayMode dm = g.getDeviceConfiguration().getDevice().getDisplayMode();
    	
    	//View view = /*field.*/getView();
		int fieldWidth = /*field.*/getWidth();
		int fieldHeight = /*field.*/getHeight();
		// dimensions of a square in pixels
		int squareWidth =(int)(dm.getWidth() / view.getWidth());
		int squareHeight = (int)(dm.getHeight() / view.getHeight());

		int positionX = 0;
		int positionY = 0;

		/**
		 *  print VIEWable part of the terrain; ie. the squares which are in the view
		 *  int x/y = absolute coordinates of squares in viewable window
		 *  int xpos/ypos  = relative position of square in view (always from 0 to viewsize)
		 */  
		
		for (int y=view.getYPosition(), ypos=0; y<y+view.getHeight()-1 && y<fieldHeight; y++, ypos++) {
			for (int x=view.getXPosition(), xpos=0; x<x+view.getWidth()-1 && x<fieldWidth; x++, xpos++) {

				Square s = /*field.*/getSquare(x, y);

				// pixel-coordinates of squares
				positionX = xpos * squareWidth;
				positionY = ypos * squareHeight;

				// draw square in the right color
				g.setColor(s.getType().getColor());                        
				g.fillRect(positionX+1, positionY+1, squareWidth-1, squareHeight-1);    

				// draw entity: building or vehicle
				if (s.getEntity() != null) {

					/* REFACTOR: entity should know how to print itself (image) */
					Entity e = s.getEntity();

					if (e instanceof Vehicle) {

						g.setColor(Color.RED);
						g.fillOval((int)(positionX+((squareWidth*0.25)/2)), 
								(int)(positionY+((squareHeight*0.25)/2)), 
								(int)(squareWidth*0.75), 
								(int)(squareHeight*0.75));
					}
					else {
						g.setColor(Color.GREEN);
						g.fillRect(positionX+4, positionY+4, 
								squareWidth-6, 
								squareHeight-6);
					}

					// draw rectangle around selected entity
					if (s.getEntity().equals(/*field.*/getSelectedEntity())) {
						g.setColor(Color.BLUE);
						g.setStroke(new BasicStroke(3));
						g.drawRect(positionX+2, positionY+2, squareWidth-4, squareHeight-4);
					}
				}
			}
		}

    }
}
