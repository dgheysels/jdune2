package model;

public class FieldGenerator {

	private static Field generatedField;
	
    public static Field generateField(int width, int height) {
        
        generatedField = new Field(width, height);
        
        for (int y=0; y<generatedField.getHeight(); y++) {
            for (int x=0; x<generatedField.getWidth(); x++) {
                
                Square s;
                
                // determine terrain-type
                double r = Math.random();
                
                if (r > 0.85) {
                    s = new Square(SquareType.WALL);
                }
                else if (r > 0.5) {
                    s = new Square(SquareType.FLAT); 
                }
                else {
                    s = new Square(SquareType.MOUNTAIN);
                }
                
                generatedField.setSquare(x, y, s);
            }
        }
        
        
        
        return generatedField;
    }
    
    public static Field getGeneratedField() {
    	return generatedField;
    }
}
