package model;

import java.awt.Color;

public enum SquareType {

    NONE(-1) {
        public Color getColor() { return Color.WHITE; }
        public boolean isWalkable() { return true; }
    },
    FLAT(2) {
        public Color getColor() { return Color.LIGHT_GRAY; }
        public boolean isWalkable() { return true; }
    },
    MOUNTAIN(5) {
        public Color getColor() { return Color.GRAY; }
        public boolean isWalkable() { return true; }
    },
    WALL(0) {
        public Color getColor() { return BLACK; }
        public boolean isWalkable() { return false; }
    };
    
    SquareType(int value) {
        this.value = value;
    }
    
    public abstract Color getColor();
    public abstract boolean isWalkable();
    
    public int getValue() { return value; }
    
    private int value;
    private static final Color BROWN = new Color(128, 64, 0);
    private static final Color GREEN = Color.GREEN;
    private static final Color BLACK = Color.CYAN.BLACK;
}
