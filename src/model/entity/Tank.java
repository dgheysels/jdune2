package model.entity;

import model.Field;

public class Tank extends Vehicle/* implements IAttack*/ {

	protected int firepower;
	protected int firerange;
	protected Entity target;
	
	public Tank(EntityName name, int firepower, int firerange)  {
		super(name, 30/*, 200*/);
		this.firepower = firepower;
		this.firerange = firerange;
	}
	
	public void setTarget(Entity target) {
		this.target = target;
	}
	
	public int getFirepower() {
		return this.firepower;
	}
	
	public int getFirerange() {
		return this.firerange;
	}
	
	/**
	 * Attack the specified target
	 */
	public void performAttack() {
		System.out.println("attacking target: " + target);
	}
	
	public Tank clone() throws CloneNotSupportedException {
		Tank t = new Tank(getName(), getFirepower(), getFirerange());
		
		return t;
	}
	
}
