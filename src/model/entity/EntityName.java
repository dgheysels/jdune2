package model.entity;

public enum EntityName {
	CONSTRUCTION_YARD(5000),
	POWER_PLANT(300),
	REFINERY(2000),
	HEAVY_FACTORY(2500),
	COMBAT_TANK(150),
	LAUNCHER(100);
	
	EntityName(int hitpoints) {
		this.hitpoints = hitpoints;
	}
	
	public int getHitpoints() {
		return this.hitpoints;
	}
	
	private int hitpoints;
}
