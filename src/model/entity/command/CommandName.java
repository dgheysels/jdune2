package model.entity.command;

public enum CommandName {

	CANCEL, MOVE, BUILD
}
