package model.entity.command;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.swing.SwingWorker;

import model.Square;
import model.entity.Entity;
import model.entity.Vehicle;

public class MoveCommand implements Command {

	private Vehicle receiver;
	
	public MoveCommand(Vehicle receiver) {
		this.receiver = receiver;
		this.receiver.setActiveCommand(this);	
	}
	
	@Override
	public SwingWorker<Entity, Integer> execute(final Square target) {
		SwingWorker<Entity, Integer> w = new SwingWorker<Entity, Integer>() {
			protected Entity doInBackground() {
				receiver.initiateGPS(target);
				receiver.performMove();
				
				return receiver;
			}
		};
		
		w.execute();
		return w;		
	}

	public void setReceiver(Vehicle receiver) {
		this.receiver = receiver;
		this.receiver.setActiveCommand(this);
	}

}
