package model.entity.command;

import javax.swing.SwingWorker;

import model.Square;
import model.entity.Entity;


/**
 * Interface for commands
 * When a command is executed, a Swingworker is started asynchronously.  
 * This method returns immediately this swingworker object, which can be used to cancel the command 
 * or to retrieve the current progress
 * 
 * When the swingworker is done (command is completed), an Entity object is returned
 * When the current progress is retrieved, an Integer object is returned
 * 
 */
public interface Command {
    

	
    SwingWorker<Entity, Integer> execute(Square target);

    
}
