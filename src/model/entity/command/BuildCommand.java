package model.entity.command;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import sun.swing.SwingUtilities2;

import model.Square;
import model.entity.Building;
import model.entity.ConstructionBuilding;
import model.entity.Entity;
import model.entity.EntityName;
import model.entity.Tank;

/* http://java.sun.com/products/jfc/tsc/articles/threads/threads2.html#example_one */

public class BuildCommand implements Command {
	
	private ConstructionBuilding receiver;
	
	public BuildCommand(ConstructionBuilding receiver) {
		this.receiver = receiver;
		receiver.setActiveCommand(this);
	}
	
	public SwingWorker<Entity, Integer> execute(Square target) {
		
		SwingWorker<Entity, Integer> w = new SwingWorker<Entity, Integer>() {
			protected Entity doInBackground() {
				

				
				// Some timeconsuming show...
				// the progress is frequently published. 
				// The GUI takes the published values to update itself (progressbar, ...)
				for (int i=0; i<receiver.getSelectedPrototype().getHitpoints(); i++) {
					
					publish(i);
					setProgress((int)((i/(float)receiver.getSelectedPrototype().getHitpoints())*100)+1);
					
					
					try {
						Thread.sleep(100);
					}
					catch(InterruptedException ex) {
						//ex.printStackTrace();
					}
					
				}
				
				
				
				
				// TODO: delegate the instantiation to the receiver
				receiver.performBuild();
												
				return receiver;
				
				
				//return null;//new Tank("new tank", 1000, 1, null);
			}
			
		};
		
		w.execute();
		return w;
		
	}
}
