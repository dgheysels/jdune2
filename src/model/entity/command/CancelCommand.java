package model.entity.command;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import javax.swing.SwingWorker;

import model.Square;
import model.entity.Entity;

public class CancelCommand implements Command {
	
	private Entity receiver;
	
	public CancelCommand(Entity receiver) {
		this.receiver = receiver;
		this.receiver.setActiveCommand(this);
	}
	
	public SwingWorker<Entity, Integer> execute(final Square target) {
		SwingWorker<Entity, Integer> w = new SwingWorker<Entity, Integer>() {
			protected Entity doInBackground() {
				if (receiver.getExecutingCommand() != null) {
					receiver.getExecutingCommand().cancel(true);
					return receiver;
				}
				
				return null;
			}
		};
		
		w.execute();
		return w;
			
			
	}
	
	public Entity getReceiver() {
		return this.receiver;
	}

}
