package model.entity;

public class Refinery extends Building {
	
	private int capacity;
	
	public Refinery() {
		super(EntityName.REFINERY/*, 2000*/);
		this.capacity = 2000;
	}
	
	public void setCapacity(int capacity) { this.capacity = capacity; }
	public int getCapacity() { return this.capacity; }
	
	public Refinery clone() throws CloneNotSupportedException {
		Refinery r = new Refinery();
		r.setCapacity(capacity);
		
		return r;
	}

}
