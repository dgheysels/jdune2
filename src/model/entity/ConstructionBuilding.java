package model.entity;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import model.entity.command.BuildCommand;
import model.entity.command.CommandName;

public class ConstructionBuilding extends Building {
	
	protected Map<EntityName, Entity> availablePrototypes = new HashMap<EntityName, Entity>();
	public Entity builtPrototype;
	
	// PROTOTYPE - PATTERN
	// When an entity is built, this prototype variable is cloned
	//protected Entity selectedPrototype;
	protected EntityName selectedPrototype;
	
	public ConstructionBuilding(EntityName name/*, int hitpoints*/) {
		super(name/*, hitpoints*/);
		supportedCommands.put(CommandName.BUILD, BuildCommand.class);
		//availablePrototypes.put(EntityName.POWER_PLANT, new PowerPlant());
		availablePrototypes.put(EntityName.REFINERY, new Refinery());
	}
	
	public EntityName getSelectedPrototype() { return this.selectedPrototype; }
	public void setSelectedPrototype(EntityName prototype) { this.selectedPrototype = prototype; }
	
//	public void installNewPrototypes(Entity[] buildings) {
//		for (Entity b : buildings) {
//			installNewPrototype(b);
//		}
//	}
	
	public void installNewPrototype(Entity building) {
		availablePrototypes.put(building.getName(), building);
	}
	
	public void setAvailablePrototypes(Map<EntityName, Entity> prototypes) {
		this.availablePrototypes = prototypes;		
	}
	
	//public Map getInstalledPrototypes() { return this.availablePrototypes; }	
	//public Entity getInstalledPrototype(String name) { return (Entity)availablePrototypes.get(name); }
	
	public /*Entity*/ void performBuild() {
		try {
			/*return*/this.builtPrototype =  availablePrototypes.get(selectedPrototype).clone();
		}
		catch(CloneNotSupportedException ex) {
			ex.printStackTrace();
			//return null;
		}
	}

	@Override
	public ConstructionBuilding clone() throws CloneNotSupportedException {
		ConstructionBuilding cb = new ConstructionBuilding(getName()/*, getHitpoints()*/);
		cb.setAvailablePrototypes(this.availablePrototypes);
		
		return cb;
	}

}
