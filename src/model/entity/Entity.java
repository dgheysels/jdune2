package model.entity;

import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingWorker;

import model.Square;
import model.entity.command.Command;
import model.entity.command.CommandName;

public abstract class Entity implements Cloneable {
	
		
	protected EntityName name;
	protected int hitpoints;
	protected Square position; 
	
	protected Map<CommandName, Class<? extends Command>> supportedCommands;

	// current executing command.  
	// The Swingworker is a background worker-thread which is the result of executing a command  
	// This swingworker can cancel the executing command in the threadpool and provide the GUI
	// with progress-information
	protected SwingWorker<Entity, Integer> executingCommand;

	// selected command
	// this is the current selected command for an entity, NOT the current executing one
	protected Command activeCommand;
	
	

	protected Entity(EntityName name/*, int hitpoints*/) {
		this.name = name;
		this.hitpoints = name.getHitpoints();//hitpoints;
		this.supportedCommands = new HashMap<CommandName, Class<? extends Command>>();
	}

	public EntityName getName() { return this.name; }

	public Square getPosition() { return this.position; }
	
	public int getHitpoints() { return this.hitpoints; }
	
	public void setPosition(Square position) { 
		this.position = position; 
		if (position.getEntity() != this) {
			position.setEntity(this);
		}
	}

	public Map<CommandName, Class<? extends Command>> getSupportedCommands() {
		return this.supportedCommands;
	}
	
	public SwingWorker<Entity, Integer> getExecutingCommand() { return this.executingCommand; }

	public Command getActiveCommand() { return this.activeCommand; }
	public void setActiveCommand(Command command) { this.activeCommand = command; }

	/**
	 * cancels the current executing command and starts the active (selected) command
	 */
	public void executeActiveCommand(Square target) {
		if (executingCommand != null) {
			executingCommand.cancel(true);
		}

		this.executingCommand = activeCommand.execute(target);
		this.activeCommand = null;
	}	
	
	@Override
	public abstract Entity clone() throws CloneNotSupportedException;
}
