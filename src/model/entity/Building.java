package model.entity;

public abstract class Building extends Entity {
	
	protected Building(EntityName name/*, int hitpoints*/) {
		super(name/*, hitpoints*/);
	}
	
	@Override
	public abstract Building clone() throws CloneNotSupportedException;
}
