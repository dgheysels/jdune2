package model.entity;

import java.util.Iterator;
import java.util.List;

import model.GPS;
import model.Square;
import model.entity.command.CancelCommand;
import model.entity.command.CommandName;
import model.entity.command.MoveCommand;

/**
 * Is responsible for driving from start to destination
 */
public abstract class Vehicle extends Entity {

	protected GPS gps;
	protected int speed;

	protected Vehicle(EntityName name, int speed/*, int hitpoints*/) {
		super(name/*, hitpoints*/);
		this.gps = new GPS();
		setSpeed(speed);
		supportedCommands.put(CommandName.MOVE, MoveCommand.class);
		supportedCommands.put(CommandName.CANCEL, CancelCommand.class);
	}

	/**
	 * This method sets the GPS ready for using.
	 * start = square where vehicle currently standing
	 * destination = target square
	 */
	public void initiateGPS(Square destination) {
		gps.setStart(getPosition());
		gps.setDestination(destination);
	}

	public GPS getGPS() { return this.gps; }

	public int getSpeed() { return this.speed; }
	public void setSpeed(int speed) { this.speed = speed; }

	/**
	 * no synchronization needed because all variables are local
	 * each vehicle has its own GPS-object
	 */
	public void performMove() {
		List<Square> path = gps.findpath();
		Iterator<Square> it = path.iterator();
		Square nextSquare = null;

		try {
			while (it.hasNext()) {
				// get next square on the path
				nextSquare = it.next();

				// determine speed here
				Thread.sleep(getPosition().getValue()*200 - speed);

				/* TODO don't drive on vehicle-occupied squares */
				
				
				// drive vehicle to next square
				driveTo(nextSquare);
			}
		}	
		catch(InterruptedException ex) {
			// InterruptedException may be thrown when (move)-command is cancelled, because of 
			// interrupting sleep()
			ex.printStackTrace();		
		}
	}

	private void driveTo(Square nextSquare) {	
		getPosition().setEntity(null);
		setPosition(nextSquare);
		
		nextSquare.notifyField();
	}
	
	@Override
	public abstract Vehicle clone() throws CloneNotSupportedException;
}
