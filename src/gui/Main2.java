package gui;

import gui.component.terrain.TerrainComponent;
import gui.component.terrain.View;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.BufferCapabilities;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import model.Field;
import model.FieldGenerator;
import model.Square;
import model.entity.Entity;
import model.entity.EntityName;
import model.entity.Tank;
import model.entity.Vehicle;

public class Main2 extends JFrame {

	private Field field;

	private BufferStrategy bufferStrategy;
	private boolean running = true;

	public Main2() {
		super("JDune 2");

		setUndecorated(true);
		setResizable(false);
		setIgnoreRepaint(true);
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					running = false;
					dispose();
					System.exit(0);
				}
			}
		});

		MouseAdapter mouseAdapter = new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				if (e.getX() <= 5) {
					field.getView().moveLeft();
				}
				else if (e.getX() >= getWidth() - 5) {
					field.getView().moveRight();
				}

				if (e.getY() <= 5) {
					field.getView().moveUp();
				}
				else if (e.getY() >= getHeight() - 5) {
					field.getView().moveDown();
				}
			}

			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() > 0) {
					field.getView().zoomIn();
				}
				else if (e.getWheelRotation() < 0) {
					field.getView().zoomOut();
				}
			}
		};

		addMouseMotionListener(mouseAdapter);
		addMouseWheelListener(mouseAdapter);

		init();

	}

	private void init() {

		field = FieldGenerator.generateField(50, 50);
		field.setView(new View(field, 20, 20));
	}	
	
	public void setup() {
		Vehicle v = new Tank(EntityName.COMBAT_TANK, 20, 1);
		
		field.addEntity(v, 3, 3);
	}

	public void start() {
		createBufferStrategy(2);
		bufferStrategy = getBufferStrategy();
		
		setup();
		
		Thread gameloop = new Thread(
				new Runnable() {
					public void run() {
						
						while (running) {
								
													
							// paint screen
							paint(bufferStrategy);
														
						}
					}
				});

		gameloop.start();
	}


	private void paint(BufferStrategy bufferStrategy) {
		Graphics2D g = (Graphics2D)bufferStrategy.getDrawGraphics();
				
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
	
		field.paint(g);

		bufferStrategy.show();
		g.dispose();
	}


	public static void main(String[] args) {

		Main2 m = new Main2();
		m.setVisible(true);

		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		try {
			gd.setFullScreenWindow(m);
			m.start();
		}
		finally {


			gd.setFullScreenWindow(null);
		}
	}
}
