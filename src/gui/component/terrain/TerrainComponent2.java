package gui.component.terrain;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Field;
import model.FieldGenerator;
import model.entity.EntityName;
import model.entity.Tank;

public class TerrainComponent2 extends JPanel {

	private Field model;
	
	public TerrainComponent2(Field model) {
		super("JDune 2");
		setUndecorated(true);
		setIgnoreRepaint(true);
		
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					dispose();
					System.exit(0);
				}
			}
		});
		
		this.model = model;
	}
	
	public void start() {
		
	}
	
	private void init() {
		setLayout(new BorderLayout());

		model = FieldGenerator.generateField(WIDTH, HEIGHT);
		model.addEntity(new Tank(EntityName.COMBAT_TANK, 50, 2), 3, 3);
		model.addEntity(new Tank(EntityName.COMBAT_TANK, 50, 2), 10, 10);
		
		TerrainComponent2 t = new TerrainComponent2(model);

		
	}
	
	public static void main(String[] args) {
		
	}
}
