package gui.component.terrain;


import java.awt.Rectangle;

import model.Field;

public class View {

	private TerrainComponent terrain;
	private Field field;
	
	// dimensions of the view
	private Rectangle viewSize;

	public View() {
		viewSize = new Rectangle();
	}

	public View(TerrainComponent terrain, int width, int height) {
		this();
		this.viewSize.x = 0;
		this.viewSize.y = 0;
		this.viewSize.width = width;
		this.viewSize.height = height;
		this.terrain = terrain;	
		this.field = terrain.getModel();
		
	}
	
	public View(Field field, int width, int height) {
		this();
		this.terrain = new TerrainComponent(field);
		this.field = field;
		this.viewSize.x = 0;
		this.viewSize.y = 0;
		this.viewSize.width = width;
		this.viewSize.height = height;
	

	}

	public int getXPosition() { return viewSize.x; }
	public int getYPosition() { return viewSize.y; }
	public int getWidth() { return viewSize.width; }
	public int getHeight() { return viewSize.height; }
	public void setWidth(int w) { viewSize.width = w; }
	public void setHeight(int h) { viewSize.height = h; }
	

	public boolean moveLeft() {
		if (getXPosition() > 0) {            
			decrementX();
			return true;
		}
		else {
			return false;
		}
	}

	public boolean moveRight() {
		if (getXPosition() + viewSize.getWidth() < /*terrain.getFieldWidth()*/field.getWidth()) {
			incrementX();            
			return true;
		}
		else {
			return false;
		}
	}

	public boolean moveUp() {
		if (getYPosition() > 0) {
			decrementY();
			return true;
		}
		else {
			return false;
		}
	}

	public boolean moveDown() {
		if (getYPosition() + viewSize.getHeight() < /*terrain.getFieldHeight()*/field.getHeight()) {
			incrementY();
			return true;
		}
		else {
			return false;
		}
	}
	
	public void zoomIn() {
		int height = getHeight();
    	int width = getWidth();
    	
        if (height > 2) {
        	setHeight(height-1);        
        }
        if (width > 2) {
            setWidth(width-1);     
        }
        
       
	}
	
	public void zoomOut() {
		int height = getHeight();
    	int width = getWidth();
    	
        if (height+1 < /*terrain.getFieldHeight()*/field.getHeight()) {
        	setHeight(height+1);     
        }
        if (width+1 < /*terrain.getFieldWidth()*/field.getWidth()) {
           setWidth(width+1);         
        }
	}



	private void incrementX() { viewSize.x++; }
	private void decrementX() { viewSize.x--; }
	private void incrementY() { viewSize.y++; }
	private void decrementY() { viewSize.y--; }

	/**
	 * returns the X/Y coordinate of the VIEWABLE square on which is clicked
	 * range = X: 0 to viewport-width
	 *         Y: 0 to viewport-height
	 */
	public int calculateXCoordinate(int mouseX) {
		
		return mouseX / ((int)(terrain.getWidth() / viewSize.getWidth()));
	}

	public int calculateYCoordinate(int mouseY) {
		return mouseY / ((int)(terrain.getHeight() / viewSize.getHeight()));
	}
}
