package gui.component.terrain;


import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;
import javax.swing.UIManager;

import model.Field;
import model.Square;
import model.entity.Entity;
import model.entity.Vehicle;

public class TerrainComponent extends JComponent implements Observer {

    private Field model;
    
    private int squareHeight;
    private int squareWidth;
        
    public TerrainComponent(Field model) {
        this.model = model;
                
        this.model.setView(new View(this, 20, 
                                          20));
        
        // OBSERVER pattern: component observes the model
        model.addObserver(this);
               
        updateUI();
    }
    
    public Field getModel() { return this.model; }
    
    public void addEntity(Entity entity, int xCo, int yCo) {
       model.addEntity(entity, xCo, yCo);
    }
    
    public void removeEntity(Entity entity) {
        model.removeEntity(entity);
    }
    
    public Square getSquare(int xCo, int yCo) {
        return model.getSquare(xCo, yCo);
    }
    
    /**
     * get field dimensions in squares
     */
    public int getFieldHeight() { return model.getHeight(); }
    public int getFieldWidth() { return model.getWidth(); }
    
    /**
     * get square dimensions in pixels
     */
    public int getSquareHeight() { return this.squareHeight; }
    public void setSquareHeight(int height) { this.squareHeight = height; }
    
    public int getSquareWidth() { return this.squareWidth; }
    public void setSquareWidth(int width) { this.squareWidth = width; }
            
    /**
     * get view on component
     */
    public View getView() { return model.getView(); }
    
    /**
     * change the position of the view
     */
    public void moveUp() {
        if (getView().moveUp()) {
            updateUI();
        }
    }
    
    public void moveRight() {
        if (getView().moveRight()) {
            updateUI();        
        }
    }
    
    public void moveDown() {
        if (getView().moveDown()) {
            updateUI();
        }
    }
    
    public void moveLeft() {
        if (getView().moveLeft()) {
            updateUI();
        }
    }
       
    /**
     * OBSERVER PATTERN
     */
    @Override
    public void update(Observable subject, Object params) {
        updateUI();
        
        /* faster update -- bugs */
//        Square updatedSquare = (Square)params;
//        repaint((updatedSquare.getXCo()-1) * squareWidth, 
//                (updatedSquare.getYCo()-1) * squareHeight, 
//                3*squareWidth, 3*squareHeight);
         
    }
    
    
    /**
     * SWING Framework methods
     */
    public void setUI(TerrainUI ui) {
        super.setUI(ui);       
    }
    
    @Override
    public String getUIClassID() {
        return "TerrainUI";
    }
    
    @Override
    public void updateUI() {
        setUI((TerrainUI)UIManager.getUI(this));
        invalidate();
    }
}
