package gui.component.terrain;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

import model.Field;
import model.Square;
import model.entity.Entity;
import model.entity.Vehicle;
import model.entity.command.MoveCommand;

public class TerrainUI extends ComponentUI implements MouseListener {

	public TerrainUI() {}

	@Override
	public void paint(Graphics g, JComponent c) {

		//long t0 = System.nanoTime();

		Graphics2D g2d = (Graphics2D)g;
		
		TerrainComponent terrainComponent = (TerrainComponent)c;
		View view = terrainComponent.getView();

		// dimensions of a square in pixels
		int squareWidth =(int)(c.getWidth() / view.getWidth());
		int squareHeight = (int)(c.getHeight() / view.getHeight());

		terrainComponent.setSquareWidth(squareWidth);
		terrainComponent.setSquareHeight(squareHeight);

		int positionX = 0;
		int positionY = 0;

		/**
		 *  print VIEWable part of the terrain; ie. the squares which are in the view
		 *  int x/y = absolute coordinates of squares in viewable window
		 *  int xpos/ypos  = relative position of square in view (always from 0 to viewsize)
		 */  		 
		for (int y=view.getYPosition(), ypos=0; y<y+view.getHeight()-1 && y<terrainComponent.getFieldHeight(); y++, ypos++) {
			for (int x=view.getXPosition(), xpos=0; x<x+view.getWidth()-1 && x<terrainComponent.getFieldWidth(); x++, xpos++) {

				Square s = terrainComponent.getSquare(x, y);

				// pixel-coordinates of squares
				positionX = xpos * squareWidth;
				positionY = ypos * squareHeight;

				// draw square in the right color
				g2d.setColor(s.getType().getColor());                        
				g2d.fillRect(positionX+1, positionY+1, squareWidth-1, squareHeight-1);    

				// draw entity: building or vehicle
				if (s.getEntity() != null) {
					
					/* REFACTOR: entity should know how to print itself (image) */
					Entity e = s.getEntity();
					
					if (e instanceof Vehicle) {
					
						g2d.setColor(Color.RED);
						g2d.fillOval((int)(positionX+((squareWidth*0.25)/2)), 
								(int)(positionY+((squareHeight*0.25)/2)), 
								(int)(squareWidth*0.75), 
								(int)(squareHeight*0.75));
					}
					else {
						g2d.setColor(Color.GREEN);
						g2d.fillRect(positionX+4, positionY+4, 
								squareWidth-6, 
								squareHeight-6);
					}

					// draw rectangle around selected entity
					if (s.getEntity().equals(terrainComponent.getModel().getSelectedEntity())) {
						g2d.setColor(Color.BLUE);
						g2d.setStroke(new BasicStroke(3));
						g2d.drawRect(positionX+2, positionY+2, squareWidth-4, squareHeight-4);
					}
				}
			}
		}

		//long t1 = (System.nanoTime() - t0);
		//System.out.println("PRINT TERRAIN TIME: " + (t1 / 1000000f));
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		TerrainComponent terrainComponent = (TerrainComponent)e.getComponent();
		Field model = terrainComponent.getModel();

		// absolute square coordinate =  position of viewport + coordinates of the viewable square in window
		int x = model.getAbsoluteXCoordinate(e.getX());
		int y = model.getAbsoluteYCoordinate(e.getY());

		Square square = model.getSquare(x, y);

		// valid square
		if (square.isWalkable()) {
			// if selected square has an entity (vehicle or building) then this entity is selected
			if (square.getEntity() != null) {  
				Entity entity = square.getEntity();

				model.setSelectedEntity(entity); 
			}
			// an empty square is clicked and an entity was previously selected --> MOVE
			else if (model.getSelectedEntity() != null) {
				Entity selectedEntity = model.getSelectedEntity();

				MoveCommand move = new MoveCommand((Vehicle)selectedEntity);
				selectedEntity.executeActiveCommand(square);
			}
			
		}
	}  

	public void mouseReleased(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}  
	
	/**
	 * SWING Framework methods for custom components
	 */
	public static ComponentUI createUI(JComponent c) {
		return new TerrainUI();
	}

	@Override
	public void installUI(JComponent c) { 
		c.addMouseListener(this);
	}

	@Override
	public void uninstallUI(JComponent c) {
		c.removeMouseListener(this);
	}
}
