package gui.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import javax.swing.border.LineBorder;

import model.Field;
import model.entity.ConstructionBuilding;
import model.entity.Entity;
import model.entity.EntityName;
import model.entity.Tank;
import model.entity.Vehicle;
import model.entity.command.BuildCommand;
import model.entity.command.CancelCommand;
import model.entity.command.Command;
import model.entity.command.CommandName;
import model.entity.command.MoveCommand;

public class DetailPanel extends JPanel implements Observer {

	private Field model;
	private JLabel vehicleName;

	private JPanel commandPanel;

	private JProgressBar progress;
	private JLabel progressLabel;

	public DetailPanel(Field model) {
		this.model = model;

		GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Rectangle windowbounds = genv.getMaximumWindowBounds();

		setPreferredSize(new Dimension(windowbounds.width/5, windowbounds.height));

		init();

		model.addObserver(this);
	}

	private void init() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		vehicleName = new JLabel();

		commandPanel = new JPanel();
		commandPanel.setLayout(new BoxLayout(commandPanel, BoxLayout.Y_AXIS));
		commandPanel.setBorder(new LineBorder(Color.RED));


		/*
		 * TODO/REFACTOR 
		 *
		 * BUILD command >
		 * 	detailpanel: 
		 * 		show all available entities + progress bar
		 *   	select entity
		 *   	BUILD Command = build entity immediately -> update progress bar
		 */

		progress = new JProgressBar(0, 100);
		progressLabel = new JLabel();



		add(vehicleName);
		add(commandPanel);

//		add(progress);
//		add(progressLabel);

	}

	/**
	 * OBSERVER PATTERN
	 * This method is called when the model is changed.
	 * Eg.: Entity selected
	 */
	@Override
	public void update(Observable subject, Object params) {
		final Field model = (Field)subject;

		if (model.getSelectedEntity() instanceof ConstructionBuilding) {
			add(progress);
			add(progressLabel);
		}
		else {
			remove(progress);
			remove(progressLabel);
		}

		vehicleName.setText(model.getSelectedEntity().getName().name());

		commandPanel.removeAll();
		commandPanel.validate();

		for (final Map.Entry<CommandName, Class<? extends Command>> c : model.getSelectedEntity().getSupportedCommands().entrySet()) {
			JButton commandButton = new JButton(c.getKey().name());
			commandButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						// some commands should execute immediately when the button is clicked 
						if (c.getKey().equals(CommandName.CANCEL)) {
							c.getValue().getConstructor(Entity.class).newInstance(model.getSelectedEntity());
							model.getSelectedEntity().executeActiveCommand(null);
						}
						else if (c.getKey().equals(CommandName.BUILD)) {

							String choice = JOptionPane.showInputDialog("choose building (eg: POWER_PLANT)...");							
							System.out.println("building " + choice);

							final ConstructionBuilding selectedBuilding = (ConstructionBuilding)model.getSelectedEntity();
							selectedBuilding.setSelectedPrototype(EntityName.valueOf(choice));


							c.getValue().getConstructor(ConstructionBuilding.class).newInstance(selectedBuilding);
							selectedBuilding.executeActiveCommand(null);

							SwingWorker<Entity, Integer> sw = selectedBuilding.getExecutingCommand();
							sw.addPropertyChangeListener(
									new PropertyChangeListener() {
										public void propertyChange(PropertyChangeEvent e) {
											if (e.getPropertyName().equals("progress")) {
												progress.setValue((Integer)e.getNewValue());												
												progressLabel.setText(String.valueOf(e.getNewValue()));
											}
										}
									});
							try {

								if (sw.isDone()) {
									progressLabel.setText("CONSTRUCTION COMPLETE");
									System.out.println("not done yet");
								}
								else {
									System.out.println("done: " + sw.get().getName());
								}

								// TODO: fix the freeze...

								//Entity entity = (Entity)sw.get();
								//System.out.println("sw is done: " + entity.getName());
							}

							catch(Exception ex) {
								ex.printStackTrace();					
							}		
						}
						else {
							c.getValue().getConstructor(Vehicle.class).newInstance(model.getSelectedEntity());
						}
					}
					catch(InstantiationException ex) {
						ex.printStackTrace();
					}
					catch(IllegalAccessException ex) {
						ex.printStackTrace();
					}
					catch(NoSuchMethodException ex) {
						ex.printStackTrace();
					}
					catch(InvocationTargetException ex) {
						ex.printStackTrace();
					}
				}
			});
			commandPanel.add(commandButton);
		}
	}


}

