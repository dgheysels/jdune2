package gui;


import gui.component.DetailPanel;
import gui.component.terrain.TerrainComponent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;

import model.Field;
import model.FieldGenerator;
import model.entity.*;

public class Main extends JFrame {

	/* TODO: use SWT instead */
	
	private static final int WIDTH = 50;
	private static final int HEIGHT = 50;

	private Field field;
	private TerrainComponent t;

	public Main() {
		super("A* Demo");


		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		init();

	}

	public void init() {

		setLayout(new BorderLayout());

		field = FieldGenerator.generateField(WIDTH, HEIGHT);

		t = new TerrainComponent(field);

		t.addEntity(new Tank(EntityName.COMBAT_TANK, 50, 2), 3, 3);
		t.addEntity(new Tank(EntityName.COMBAT_TANK, 50, 2), 10, 10);
//		ConstructionBuilding heavyFactory = new ConstructionBuilding(EntityName.HEAVY_FACTORY);
//		heavyFactory.installNewPrototype(new Tank(EntityName.COMBAT_TANK, 15, 2));
//		heavyFactory.installNewPrototype(new Tank(EntityName.LAUNCHER, 90, 10));	
//		heavyFactory.setSelectedPrototype(EntityName.LAUNCHER);				
//		
//		ConstructionBuilding constructionYard = new ConstructionBuilding(EntityName.CONSTRUCTION_YARD);
//		constructionYard.installNewPrototype(heavyFactory);
//		//constructionYard.setSelectedPrototype(constructionYard.getInstalledPrototype(EntityName.HEAVY_FACTORY));
		
		//ConstructionBuilding heavyFactory2 = (ConstructionBuilding)constructionYard.performBuild();
		
		///*Entity launcher = */heavyFactory.performBuild();
		//t.addEntity(/*launcher*/heavyFactory.builtPrototype, 3, 3);
			
		//heavyFactory.setSelectedPrototype(EntityName.COMBAT_TANK);
		//heavyFactory.performBuild();
		//t.addEntity(heavyFactory.builtPrototype, 10, 8);
		
		//t.addEntity(constructionYard, 1, 1);
		//t.addEntity(heavyFactory2, 1, 2);
		
		JButton up = new JButton("UP");
		up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t.moveUp();
			}
		});

		JButton left = new JButton("LEFT");
		left.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t.moveLeft();
			}
		});

		JButton right = new JButton("RIGHT");        
		right.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t.moveRight();                
			}
		});

		JButton down = new JButton("DOWN");
		down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t.moveDown();
			}
		});

		JPanel terrainPanel = new JPanel();
		terrainPanel.setLayout(new BorderLayout());
		terrainPanel.add(t, BorderLayout.CENTER);
		terrainPanel.add(up, BorderLayout.NORTH);
		terrainPanel.add(left, BorderLayout.WEST);
		terrainPanel.add(right, BorderLayout.EAST);
		terrainPanel.add(down, BorderLayout.SOUTH);

		add(terrainPanel, BorderLayout.CENTER);

		
		DetailPanel detailPanel = new DetailPanel(field);
		
				
		add(detailPanel, BorderLayout.EAST);

		
		JPanel buttons = new JPanel();
		buttons.setBorder(new EtchedBorder());
	
		JButton zoomin = new JButton("+");
        zoomin.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

            	int height = t.getView().getHeight();
            	int width = t.getView().getWidth();
            	
                if (height > 1) {
                	t.getView().setHeight(height-1);
                    t.repaint();
                }
                if (width > 1) {
                    t.getView().setWidth(width-1);
                    t.repaint();
                }

            }
        });   

		JButton zoomout = new JButton("-");    
        zoomout.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

            	int height = t.getView().getHeight();
            	int width = t.getView().getWidth();
            	
                if (height < field.getHeight()) {
                	t.getView().setHeight(height+1);
                    t.repaint();
                }
                if (width < field.getWidth()) {
                    t.getView().setWidth(width+1);
                    t.repaint();
                }


            }
        });
		    
		buttons.add(zoomin);
		buttons.add(zoomout);

		add(buttons, BorderLayout.SOUTH);       
	}



	public static void main(String[] args) {
		UIManager.put("TerrainUI", "gui.component.terrain.TerrainUI");

		Main t = new Main();
		t.setVisible(true);
		//GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		//gd.setFullScreenWindow(t);
		
	}
}
